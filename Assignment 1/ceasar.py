import platform  # For OS check
from os import system  # For clear/cls command
import time  # for time.sleep


def menu():
    print('Write the corresponding number to make your choice!')
    print('1) Input a text to the file?')
    print('2) Show the text currently in the file?')
    print('3) Encrypt text already in the file?')
    print('4) Decrypt the text?')
    print('5) Quit?')
    choice = int(input())
    while choice < 1 or choice > 5:
        choice = int(input('Value not acceptable, input numbers 1 ≤ X ≤ 5\n'))
    return(str(choice))


def encrypt(msg, key):
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    cipher = ''
    for letter in msg:          # Loops through word
        for x in range(26):     # Loops through alpabet
            if letter == ' ':   # Stops looking for letters if it's a space
                cipher += ' '
                break
            elif letter == alphabet[x]:  # Compares current letter to alphabet
                num = x + int(key)       # Moves letters in msg forward
                num = (num % 26)         # Checks if the letter is moved beyond limit
                cipher += alphabet[num]  # Builds the cipher
    return(cipher)


def decrypt(cipher, key):
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
               'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    plain_text = ''
    for letter in cipher:
        for x in range(26):
            if letter == ' ':
                plain_text += ' '
                break
            elif letter == alphabet[x]:
                num = (x - int(key)) % -26  # Returns letter to last position
                if num < 0:
                    num += 26  # Pushes letter back into "alphabet" range
                plain_text += alphabet[num]
    return(plain_text)


def clear():
    if platform.system() == 'Windows':
        system('cls')
    else:
        system('clear')


choice = ''
while choice != '5':   # Loop that allows repeated choices by user
    choice = menu()    # Reopens menu after choice

    if choice == '1':  # Allows modification of file
        with open('file.txt', 'w') as f:
            f.write(input('Input your text:\n').upper())

    elif choice == '2':  # Allows reading of file
        with open('file.txt', 'r') as f:
            print('This is the text within your file: \n' + f.read() + '\n')
        time.sleep(2)

    elif choice == '3':  # Allows encryption of file based on key
        with open('file.txt', 'r+') as f:
            temp = f.read()
            f.seek(0)  # returns writing to start of file.txt
            f.write(encrypt(temp, input('Input a key to encrypt with!\n')))

    elif choice == '4':
        with open('file.txt', 'r+') as f:
            temp = f.read()
            f.seek(0)
            f.write(decrypt(temp, input('Input the same key to decrypt!\n')))
    clear()
