def recursive_sumrange(start, stop):
    if start == stop:
        return start
    elif start < stop:
        return stop + recursive_sumrange(start, stop - 1)
    elif start > stop:
        return stop + recursive_sumrange(start, stop + 1)

print(recursive_sumrange(2, 7))
