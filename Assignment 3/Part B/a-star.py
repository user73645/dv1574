"""Module for utilizing A* on a 2D grid"""
import math
from grid import Occupation
def a_star(grid, start, goal):  # Function to calculate a path through a grid
    openSet = [start]
    cameFrom = {}
    gScore = {}
    gScore[start] = 0
    fScore = {}
    fScore[start] = h(start, goal)
    empty = []
    blocked_cells = []

for x in range(len(grid)):
    for y in range(len(grid)):
        if grid[x][y] == Occupation.BLOCKED:
            blocked_cells.append((x, y))

    while openSet is not empty:
        current = openSet[0]  # lowSet(openSet, fScore)
        if current == goal:
            return reconstruct_path(cameFrom, current)

        openSet.remove(current)
        for neighbour in findNeighbour(current):
            tentative_gScore = gScore[current] + 1

            try:
                gScore[neighbour]
            except:
                gScore[neighbour] = math.inf

            if tentative_gScore < gScore[neighbour]:
                cameFrom[neighbour] = current
                gScore[neighbour] = tentative_gScore
                fScore[neighbour] = gScore[neighbour] + h(neighbour, goal)
                if neighbour not in openSet and neighbour not in blocked_cells:
                    openSet.append(neighbour)
    return

def h(current, end):
    left = (end[0] - current[0]) ** 2
    right = (end[1] - current[1]) ** 2
    dist = math.sqrt(left + right)
    return dist


def reconstruct_path(cameFrom, current):
    total_path = [current]
    while current in cameFrom.keys():
        current = cameFrom[current]
        total_path.insert(0, current)
    return total_path


def lowSet(openSet, fScore):
    min = 0
    for tup in openSet:
        if min < fScore[tup]:
            min = fScore[tup]
    return min


def findNeighbour(current):
    right = (current[0] + 1, current[1])
    left = (current[0] - 1, current[1])
    up = (current[0], current[1] + 1)
    down = (current[0], current[1] - 1)
    neighbours = [right, left, up, down]
    for x in neighbours:
        if x[0] < 0 or x[1] < 0 or x[0] > 14 or x[1] > 14:
            neighbours.remove(x)
    return neighbours
