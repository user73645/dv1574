import random


def numgen():
    answer = []
    while len(answer) != 4:              # While answer isn't full
        num = str(random.randint(0, 9))  # Generate number
        if num not in answer:            # If not already in -
            answer.append(num)           # add it
    return answer


def correct(guess, ans):
    full_correct = 0
    for x in range(len(ans)):       # Go through answer
        if ans[x] == guess[x]:      # Compare guess and answer
            full_correct += 1       # increase for perfect matches
    return full_correct


def wrong(guess, ans):
    partial_correct = 0
    for x in range(len(ans)):
        if guess[x] in ans:          
            partial_correct += 1    # Increase amount of partial guesses
    return partial_correct


tries = 0
guess = []
ans = numgen()

while guess != ans:             # Check for win status
    guess = input('Guess my secret code (4 numbers 0-9): ').rsplit()
    fc = correct(guess, ans)    # Alias for code cleanup 
    pc = wrong(guess, ans)      # Alias for code cleanup 
    if 0 < fc:
        print(f'''Your guess had {fc} numbers in correct position and {pc - fc}
         in wrong position''')  
    else:                      
        print(f'''Your guess had {fc} numbers in correct position and {pc} in
         wrong position''')
    tries += 1
print(f'Congratulations, you succeeded in {tries} guesses')

