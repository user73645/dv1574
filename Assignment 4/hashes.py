import hashlib
import os
import sys


def ls_a(dir, paths):                   # Lists all
    list = os.listdir(dir)
    for x in list:
        path = dir + '/' + x
        if os.path.isdir(path):
            ls_a(path, paths)
        else:
            paths.append(path)
    return paths


def hashfilegen(paths, dir):  # Create new hashfile, overwriting others
    with open('hashes.txt', 'w') as w:
        for x in paths:
            with open(x, 'rb') as f:            # ReadBytes för bin
                hash = (hashlib.sha512(f.read())).hexdigest()
                w.write(x + '\n' + hash + '\n')


def compare(newhash, oldhash):    # Print's changes
    edited = []
    for x in range(0, len(newhash), 2):
        if newhash[x] not in oldhash:         # What's new?
            print(newhash[x] + ' was added')
        elif newhash[x+1] not in oldhash:
            edited.append(newhash[x])
            print(newhash[x] + ' was edited')

    for x in range(0, len(oldhash), 2):
        if oldhash[x] not in newhash:
            print(oldhash[x] + ' was removed')
        elif oldhash[x+1] not in newhash and oldhash[x] not in edited:
            edited.append(oldhash[x])
            print(oldhash[x] + ' was edited')


def grab_lines(file):       # Converts lines to elements
    hashlist = []
    with open(file) as f:
        for x, line in enumerate(f):
            hashlist.append(line[:-1])
    return hashlist


if len(sys.argv) > 0:
    mydir = sys.argv[1]
    lst = []
    if os.path.exists(mydir):
        if os.path.exists('hashes.txt'):
            oldhash = grab_lines('hashes.txt')
            hashfilegen(ls_a(mydir, lst), mydir)
            newhash = grab_lines('hashes.txt')
            compare(newhash, oldhash)
        else:
            hashfilegen(ls_a(mydir, lst), mydir)
            print('A hashfile has been generated')
    else:
        print('Not a valid directory!')
 
